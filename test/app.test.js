"use strict";
const request = require('supertest');
const app = require('../server/app')

/* test root path */
describe('Test the root path', function() {
    test('It should response the GET method', function() {
        return request(app).get("/").then(function(response) {
            expect(response.statusCode).toBe(200);
        })
    });
});
