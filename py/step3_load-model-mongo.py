import csv
import json
import pandas as pd
import sys, getopt, pprint
from pymongo import MongoClient
csvfile = open('output.csv', 'r')
reader = csv.DictReader( csvfile )
client = MongoClient('mongodb://localhost:27017/stackup-bca')
db = client['stackup-bca']
db.rvtmodels.drop()
header= [ "__externalId","__name","__objectid","__properties_Energy Analysis_Energy Settings","__properties_Identity Data_Author","__properties_Identity Data_Building Name","__properties_Identity Data_Organization Description","__properties_Identity Data_Organization Name","__properties_Other_Client Name","__properties_Other_Project Address","__properties_Other_Project Issue Date","__properties_Other_Project Name","__properties_Other_Project Number","__properties_Other_Project Status"]

for each in reader:
    row={}
    for field in header:
        row[field]=each[field]

    db.rvtmodels.insert(row)
