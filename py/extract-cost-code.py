from pymongo import MongoClient
import os
import pyexcel as pe
import datetime

client = MongoClient('mongodb://localhost:27017/stackup-bca')
db = client['stackup-bca']


def main(base_dir):
    # Simply give a name to the Book class
    project_name="Project-A"
    project_ref_no="ABC123"
    
    book = pe.get_book(file_name=os.path.join(base_dir, "docs/CP 97-1-2002 (2015).xls"))

    # the default iterator for a **Book* instance is a SheetIterator
    for sheet in book:
        # Each sheet has name
        print("sheet: %s" % sheet.name)
        # Once you have a sheet instance, you can regard it as
        # a Reader instance. You can iterate its member in the way
        # you wanted it
        for row in sheet:
            #print(row)
            if(row[3] == 0 and row[4] == 0):
                print(row[3])
                print(row[4])
                print(row[6])
                if(row[6] != ""):
                    db.CostCodeCat.insert_one({
                        "L2": row[2],
                        "L3": row[3],
                        "L4": row[4],
                        "Category": row[6]
                    });
        
    categories2 = []
    categories = db.CostCodeCat.find({})
    for category in categories:
        print(category)
        categories2.append(category['L2'])
        
    for sheet in book:
        # Each sheet has name
        print("sheet: %s" % sheet.name)
        # Once you have a sheet instance, you can regard it as
        # a Reader instance. You can iterate its member in the way
        # you wanted it
        for row in sheet:
            #print(row)
            if row[2] in categories2:
                print(row[3])
                print(row[4])
                print(row[6])
                if(int(row[3]) > 0 and int(row[4]) > 0):
                    db.CostCode.insert_one({
                        "L2": row[2],
                        "L3": row[3],
                        "L4": row[4],
                        "Cost_Code_Desc": row[6],
                        "unit": 0,
                        "unit-measurement": "",
                        "price": 0
                    });

                    db.ProjectCostCode.insert_one({
                        "L2": row[2],
                        "L3": row[3],
                        "L4": row[4],
                        "Cost_Code_Desc": row[6],
                        "unit": 0,
                        "unit-measurement": "",
                        "price": 0,
                        "project": project_name,
                        "project_ref_no": project_ref_no,
                        "date_added": datetime.datetime.utcnow()
                    });
        

if __name__ == '__main__':
    main(os.getcwd())
