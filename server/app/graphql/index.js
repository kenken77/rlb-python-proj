'use strict';

const GraphQL = require('graphql');
const {
	GraphQLObjectType,
	GraphQLSchema,
} = GraphQL;

const UserQuery = require('./queries/User');
const UserMutation = require('./mutations/User');

const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	description: 'This is the default root query provided by the backend',
	fields: {
		users: UserQuery.index(),
		user: UserQuery.single(),
		current_user: UserQuery.current(),

    },
});


const RootMutation = new GraphQLObjectType({
	name: 'Mutation',
	description: 'Default mutation provided by the backend APIs',
	fields: {
		addUser: UserMutation.create(),
		updateUser: UserMutation.update(),
    },
});

// export the schema
module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation: RootMutation,
});