'use strict';

const Response = {

	general( data ) {
		return data;
	},

	error(code, message, description) {
		return {
			code: code || 400,
			message: message || 'some error occoured',
			description: description || 'error occoured on server, please try again after some time.',
		}
	},


	authError() {
		return Response.error(
			403,
			'authentication error',
			'no authentication token provided, please login first and provide the authentication token.'
		);
	},
}

module.exports = Response;