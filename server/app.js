/**
 * Server side code.
 */
"use strict";
const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const mongoose = require('mongoose');
const expressGraphQL = require('express-graphql');
const GraphQLSchema = require('./app/graphql');
const config = require('./app/common/config');
const AccessValidatorMiddleware = require('./app/common/middlewares/AccessValidator');

var app = express();
app.disable('x-powered-by');
app.use( cors({ optionsSuccessStatus: 200 }) );
app.use( bodyParser.json({ limit: '50mb' }) );
app.use( bodyParser.urlencoded({ limit: '50mb', extended: true }) );
//app.use( AccessValidatorMiddleware );

mongoose.Promise = require('bluebird');
mongoose.connect( config.database.HOST,  {
	socketTimeoutMS: 0,
	keepAlive: true,
	reconnectTries: 30,
	user: config.database.USERNAME,
	pass: config.database.PASSWORD,
	useMongoClient: true
  });

const NODE_PORT = config.server.PORT;

app.use(
	'/graphql',
	expressGraphQL( () => {
		return {
			graphiql: false,
			schema: GraphQLSchema,
		}
	})
);

app.use(express.static(__dirname + "/../client/"));

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app